package ru.intervi.msgcensor;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import javax.annotation.Nonnull;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Plugin(
        id = "msgcensor", name = "MsgCensor", version = "1.0",
        authors = {"InterVi"},
        description = "Add censure to server."
)
public class Main {
    @Inject
    @DefaultConfig(sharedRoot = true)
    private Path defaultConfig;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    private CommentedConfigurationNode config;

    private final String DOMAIN = "((http:\\\\/\\\\/|https:\\\\/\\\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\\\.)" +
            "{1,4}([a-zA-Z]){2,6}(\\\\/([a-zA-Z-_\\\\/\\\\.0-9#:?=&;,]*)?)?)";
    private final Set<String> REGEXP = new HashSet<>();
    private final Set<String> WORDS = new HashSet<>();
    private final Set<String> COMMANDS = new HashSet<>();

    private final String PERM_RELOAD = "msgcensor.cmd.admin.reload";
    private final String PERM_BYPASS = "msgcensor.bypass";

    private void setupConfig() {
        try {
            if (!Files.exists(defaultConfig)) {
                Files.createFile(defaultConfig);
                config = configManager.createEmptyNode();
                config.getNode("enabled").setValue(true);
                config.getNode("censure", "regex").setValue(Collections.singletonList(DOMAIN));
                config.getNode("censure", "words").setValue(Collections.singletonList("fuck"));
                config.getNode("censure", "commands").setValue(Collections.singletonList("tell"));
                config.getNode("translate", "regexp").setValue("no spam, please");
                config.getNode("translate", "words").setValue("no swear, please");
                configManager.save(config);
            } else {
                config = configManager.load();
                List<String> regex = config.getNode("censure", "regex").getList(TypeToken.of(String.class));
                List<String> words = config.getNode("censure", "words").getList(TypeToken.of(String.class));
                List<String> commands = config.getNode("censure", "commands").getList(TypeToken.of(String.class));
                REGEXP.addAll(regex);
                for (String line : words) WORDS.add(line.toLowerCase());
                for (String line : commands) COMMANDS.add(line.toLowerCase());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        setupConfig();
        CommandSpec reload = CommandSpec.builder()
                .description(Text.of("Config reload command."))
                .permission(PERM_RELOAD)
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    REGEXP.clear();
                    WORDS.clear();
                    COMMANDS.clear();
                    setupConfig();
                    src.sendMessage(Text.of("config reloaded"));
                    return CommandResult.success();
                })
                .build();
        Sponge.getCommandManager().register(this, reload, "msgc-reload", "msgcensor-reload");
    }

    private boolean containsRegexp(String msg) {
        for (String regexp : REGEXP) {
            if (msg.matches(regexp)) {
                return true;
            }
        }
        return false;
    }

    private boolean containsWords(String msg) {
        String[] parts = msg.toLowerCase().split(" ");
        for (String str : parts) {
            if (WORDS.contains(str)) {
                return true;
            }
        }
        return false;
    }

    @Listener
    public void onMsg(MessageChannelEvent.Chat event) {
        if (!config.getNode("enabled").getBoolean()) return;
        if (!(event.getSource() instanceof Player)) return;
        Player player = (Player) event.getSource();
        if (player.hasPermission(PERM_BYPASS)) return;
        String msg = event.getRawMessage().toPlain().trim();
        if (containsRegexp(msg)) {
            player.sendMessage(Text.of(config.getNode("translate", "regexp").getString()));
            event.setMessageCancelled(true);
        } else if (containsWords(msg)) {
            player.sendMessage(Text.of(config.getNode("translate", "words").getString()));
            event.setMessageCancelled(true);
        }
    }

    @Listener
    public void onCmd(SendCommandEvent event) {
        if (!config.getNode("enabled").getBoolean()) return;
        if (!(event.getSource() instanceof Player)) return;
        if (!COMMANDS.contains(event.getCommand().toLowerCase())) return;
        Player player = (Player) event.getSource();
        if (player.hasPermission(PERM_BYPASS)) return;
        String msg = event.getArguments();
        if (containsRegexp(msg)) {
            player.sendMessage(Text.of(config.getNode("translate", "regexp").getString()));
            event.setCancelled(true);
        } else if (containsWords(msg)) {
            player.sendMessage(Text.of(config.getNode("translate", "words").getString()));
            event.setCancelled(true);
        }
    }
}
